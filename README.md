# Buggy Codes by Payatu 

## Contributions
Refer to [CONTRIBUTING.md](./CONTRIBUTING.md) for all the details regarding the contributions.

## Note
In case of any query/issue, create an [issue](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#create-a-new-issue) in GitLab.