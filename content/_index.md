---
title : "Buggy Codes By Payatu"
description: "Buggy Codes is a platform containing a wide range of challenges in different languages. The purpose of the challenges are to find the security vulnerability in the code snippets."
lead: "Buggy Codes is a platform containing a wide range of challenges in different languages. The purpose of the challenges are to find the security vulnerability in the code snippets."
date: 2020-10-06T08:47:36+00:00
lastmod: 2020-10-06T08:47:36+00:00
draft: false
images: []
---
