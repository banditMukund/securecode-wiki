---
title: Prajyot Chemburkar
image: 
    url: 
    alt: 
website: 
socials:
    linkedin: 
    twitter: 
    github: 
draft: false # Change it to false if you want it published.
# Do not change the below values.
type: contributors
layout: single
# Note: Always use https:// whenever putting up links. For e.g., https://payatu.com
# All the fields above are optional
---

Prajyot is an experienced Security Consultant in Payatu. Experienced in pentesting Web Application, Mobile Application, Thick and Thin Client application, Enterprise Network and Cloud environment.
<!-- Your description here! -->
