---
title: Vaibhav Rajput
image: 
    url: 
    alt: 
socials:
    linkedin: 
    twitter: 
    github: 
draft: false
# Do not change the below values.
type: contributors
layout: single
---

Vaibhav is an experienced Security Consultant in Payatu. Experienced in pentesting Web Application, Mobile Application, Thick and Thin Client application, Enterprise Network and Cloud environment.
