---
title: Hari Prasad
image: 
    url: 
    alt: 
website: 
socials:
    linkedin: 
    twitter: 
    github: 
draft: false # Change it to false if you want it published.
# Do not change the below values.
type: contributors
layout: single
# Note: Always use https:// whenever putting up links. For e.g., https://payatu.com
# All the fields above are optional
---
Hari is a Red Team Researcher at Payatu. He specializes in advanced assessments of Mobile Security (Android and iOS), Network Infrastructure Security, DevSecOps, Container security, Web security, and Cloud security.
<!-- Your description here! -->
