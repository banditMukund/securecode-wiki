---
title: "A01:2021-Broken Access Control"
description: ""
lead: "A01:2021-Broken Access Control"
draft: false
images: []
menu:
  docs:
    parent: "docs"
toc: true
authors: Sourov Ghosh
---
## Challenge

### Challenge Name

- Description/Hint

```csharp
Challenge code

```

## Solution
```csharp
Solution code
```

## References

- [https://cheatsheetseries.owasp.org/](https://cheatsheetseries.owasp.org/)
- [https://docs.microsoft.com/en-us/dotnet/standard/security/](https://docs.microsoft.com/en-us/dotnet/standard/security/)
- [https://github.com/guardrailsio/awesome-dotnet-security](https://github.com/guardrailsio/awesome-dotnet-security)
- [https://owasp.org/www-project-top-ten/2017/Top_10.html](https://owasp.org/www-project-top-ten/2017/Top_10.html)
